//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : FS.h                                                          //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <vector>
#include <string>

//----------------------------------------------------------------------------//
// FS                                                                         //
//----------------------------------------------------------------------------//
namespace FS {

//------------------------------------------------------------------------------
bool IsFile(std::string const &path);
bool IsDirectory(std::string const &path);

std::vector<std::string> GetFiles(std::string const &path, bool recursive);

} // namespace FS
