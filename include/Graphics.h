//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : Graphics.h                                                    //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <vector>
#include <string>
// SDL
#include <SDL.h>
#include <SDL_image.h>
// ssp
#include "../include/Utils.h"

//----------------------------------------------------------------------------//
// Graphics                                                                   //
//----------------------------------------------------------------------------//
namespace Graphics {

//------------------------------------------------------------------------------
struct Image
{
    std::string  filename;
    Rect         rect;
    uint32_t     textureId;
}; // struct Image

//------------------------------------------------------------------------------
struct InitOptions
{
    uint32_t    windowWidth;
    uint32_t    windowHeight;
    char const *windowCaption;
    bool        headless;
}; // struct InitOptions


//------------------------------------------------------------------------------
void Initialize(InitOptions const &options);
void Shutdown();

//------------------------------------------------------------------------------
void PresentTexture(SDL_Texture *pTexture);

//------------------------------------------------------------------------------
SDL_Texture* GenerateOutputTexture(std::vector<Image> const &images, uint32_t width, uint32_t height);
void SaveTextureToFile(std::string const &filename, SDL_Texture *pTexture);

//------------------------------------------------------------------------------
void SafeDestroySurface(SDL_Surface *&pSurface);
void SafeDestroyTexture(SDL_Texture *&pTexture);


//------------------------------------------------------------------------------
Image LoadImageFromFile(std::string const &filename);
void FreeImage(Image &image);


} // namespace Graphics
