//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : Packer.h                                                      //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <stdint.h>
#include <vector>
// ssp
#include "../include/Graphics.h"

//----------------------------------------------------------------------------//
// Packer                                                                     //
//----------------------------------------------------------------------------//
namespace Packer {

//------------------------------------------------------------------------------
struct Options
{
    int marging;
}; // struct Options

//------------------------------------------------------------------------------
struct SpriteSheet
{
    uint32_t width;
    uint32_t height;
}; // struct SpriteSheet;

//------------------------------------------------------------------------------
Packer::SpriteSheet Pack(std::vector<Graphics::Image> &images, Options const &options);

} // namespace Packer
