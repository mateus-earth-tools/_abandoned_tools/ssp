#pragma once
// std
#include <vector>
#include <string>
// ssp
#include "Graphics.h"

namespace Exporter
{
enum class ExportType {
    PIXI
}; // enum ExportType

//------------------------------------------------------------------------------
void SaveRectsToFile(ExportType type, std::string const &filename, std::vector<Graphics::Image> images);

} // namespace Exporter
