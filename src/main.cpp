//~---------------------------------------------------------------------------//
//                        __      __                  __   __                 //
//                .-----.|  |_.--|  |.--------.---.-.|  |_|  |_               //
//                |__ --||   _|  _  ||        |  _  ||   _|   _|              //
//                |_____||____|_____||__|__|__|___._||____|____|              //
//                                                                            //
//  File      : main.cpp                                                      //
//  Project   : ssp                                                           //
//  Date      : Sep 15, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

// std
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iterator>
#include <vector>

// ssp
#include "../include/FS.h"
#include "../include/Graphics.h"
#include "../include/Utils.h"
#include "../include/Packer.h"
#include "../include/Exporter.h"

//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
void
ShowHelp(int errorCode)
{
    printf("Help: \n");
    exit(errorCode);
}


//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
int
main(int argc, char* argv[])
{
    if(argc < 2) {
        ShowHelp(1);
    }

    //
    // Get all the filenames that we'll pack in the pTexture.
    std::vector<std::string> filenames_to_process;
    filenames_to_process.reserve(argc);
    for(auto i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        if(FS::IsFile(arg)) {
            filenames_to_process.push_back(arg);
        } else if(FS::IsDirectory(arg)) {
            auto const &dir_entries = FS::GetFiles(arg, true);
            for(auto const &entry : dir_entries) {
                filenames_to_process.push_back(entry);
            }
        }
    }

    //
    // Init Graphics
    Graphics::InitOptions init_options;
    init_options.windowWidth   = 800;
    init_options.windowHeight  = 600;
    init_options.windowCaption = "ssp v0.0.1";
    init_options.headless      = false;
    Graphics::Initialize(init_options);

    //
    // Load the images that we need to pack.
    std::vector<Graphics::Image> images;
    images.reserve(filenames_to_process.size());
    for(auto const &filename : filenames_to_process) {
        images.push_back(Graphics::LoadImageFromFile(filename));
    }


    //
    // Process the images.
    Packer::Options options;
    options.marging = 0;

    auto sprite_sheet = Packer::Pack(images, options);
    auto p_output_texture = Graphics::GenerateOutputTexture(images, sprite_sheet.width, sprite_sheet.height);
    Graphics::SaveTextureToFile("/Users/stdmatt/Desktop/test.png", p_output_texture);
    Graphics::SafeDestroyTexture(p_output_texture);
    
    Exporter::SaveRectsToFile(Exporter::ExportType::PIXI, "/Users/stdmatt/Desktop/test.json", images);
    
    // SDL_Event event;
    // while (1) {
    //     SDL_PollEvent(&event);
    //     if (event.type == SDL_QUIT) {
    //         return;
    //     }

    //     else if( event.type == SDL_KEYDOWN ) {
    //         switch(event.key.keysym.sym )
    //         {
    //             case SDLK_UP   : { options.marging++; dirty = true; } break;
    //             case SDLK_DOWN : { options.marging--; dirty = true; } break;
    //         }
    //     }

    //     if(options.marging < 0) {
    //         options.marging = 0;
    //     }


    //     if(dirty) {
    //         Graphics::SafeDestroyTexture(p_output_texture);
    //         auto sprite_sheet = Packer::Pack(images, options);
    //         p_output_texture = Graphics::RenderOutputTexture(sprite_sheet);
    //     }


    //     Graphics::PresentTexture(p_output_texture);
    // }
}
